/* 
 * File:   main.cpp
 * Author: archimed
 * 
 * Created on 20 августа 2018 г., 9:34
 */

#include <iostream>
#include <fstream>
#include <regex>

#include <boost/program_options.hpp>

#include "tcp/Server.h"
#include "ResourceMgr.h"
#include "RouteTable.h"

namespace po = boost::program_options;

typedef struct RouteTable::route routeEntry;

std::map<std::uint64_t, routeEntry> readRouteTable(std::ifstream& f)
{
    std::map<std::uint64_t, routeEntry> result;

    for (std::string line; std::getline(f, line);)
    {
        std::uint64_t id;
        std::string host, port;

        std::istringstream iss(line);
        iss >> id >> host >> port;
        result.emplace(id, routeEntry{host, port});
    }
    return std::move(result);
}

int main(int argc, char *argv[])
{
    std::string routeConfig;
    std::string serverHost, serverPort;

    po::options_description opt("TCPProxy options");
    opt.add_options()
            ("addr,a", po::value(&serverHost)->default_value("0.0.0.0"), "server address")
            ("port,p", po::value(&serverPort)->default_value("8888"), "server port")
            ("route,r", po::value(&routeConfig)->required(), "route table file")
            ("help,h", "produce help message");

    po::variables_map vm;
    try
    {
        po::store(po::parse_command_line(argc, argv, opt), vm);
        po::notify(vm);
    }
    catch (std::exception& exc)
    {
        if (vm.count("help") == 1)
        {
            std::cout << opt << std::endl;
            return 0;
        }
        else
        {
            std::cerr << exc.what() << std::endl;
            return -1;
        }
    }

    if (vm.count("help") == 1)
    {
        std::cout << opt << std::endl;
        return 0;
    }

    if (routeConfig.empty())
    {
        std::cerr << "route table is empty" << std::endl;
        return -1;
    }

    std::ifstream routeF(routeConfig.c_str());
    if (!routeF)
    {
        std::cerr << "Unable open file " << routeConfig << std::endl;
        return -1;
    }

    auto routeTable = readRouteTable(routeF);

    std::cout << "Routes: " << std::endl;
    std::cout << "========================================" << std::endl;
    for (auto &i : routeTable)
        std::cout << i.first << " -> " << i.second.host << ":" << i.second.port << std::endl;

    RouteTable::instance().update(routeTable);


    auto server = std::make_shared<tcp::server::Server> (ResourceMgr::instance().getExecutor(), serverHost, serverPort);
    server->start();
    ResourceMgr::instance().run();

    return 0;
}
