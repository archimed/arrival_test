/* 
 * File:   RouteTable.cpp
 * Author: archimed
 * 
 * Created on 27 августа 2018 г., 16:03
 */

#include "RouteTable.h"
#include "ResourceMgr.h"

//==============================================================================

RouteTable::RouteTable()
{
    ResourceMgr::instance().registerOnStopCallback([this]()
    {
        stopAll();
    });
}

//==============================================================================

void RouteTable::stopAll()
{
    boost::upgrade_lock<boost::shared_mutex> lock(m_Lock);
    boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);

    m_Routes.clear();
    for (auto& i : m_Clients)
        i.second->askStop();
    m_Clients.clear();
}

//==============================================================================

void RouteTable::update(const std::map<std::uint64_t, struct route>& routes)
{
    boost::upgrade_lock<boost::shared_mutex> lock(m_Lock);
    boost::upgrade_to_unique_lock<boost::shared_mutex> uniqueLock(lock);

    for (const auto& i : routes)
    {
        auto clientID = i.first;
        const auto& route = i.second;

        auto clientIt = m_Clients.find(route);
        if (clientIt == m_Clients.end())
        {
            clientIt = m_Clients.emplace(route,
                                         std::make_shared<tcp::client::Client>(ResourceMgr::instance().getExecutor(),
                                                                               route.host,
                                                                               route.port)
                                         ).first;
            (*clientIt).second->connect();
        }

        auto& client = (*clientIt).second;
        m_Routes[clientID] = client;
    }
}

//==============================================================================

bool RouteTable::isValidId(std::uint64_t id) const
{
    boost::shared_lock<boost::shared_mutex> lock(m_Lock);
    return (m_Routes.find(id) != m_Routes.end());
}

//==============================================================================

void RouteTable::route(std::uint64_t id, const protocol::t_Buffer& msg)
{
    {
        boost::shared_lock<boost::shared_mutex> lock(m_Lock);
        m_Routes.at(id)->send(msg);
    }
}

//==============================================================================
