/* 
 * File:   ResourceMgr.h
 * Author: archimed
 *
 * Created on 27 августа 2018 г., 17:33
 */

#pragma once

#include <vector>
#include <thread>
#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>

class ResourceMgr : private boost::noncopyable
{
public:

    static ResourceMgr& instance()
    {
        static ResourceMgr _inst;
        return _inst;
    }

    boost::asio::io_service& getExecutor();
    void run();
    void registerOnStopCallback(const std::function<void()>& cb);

private:
    ResourceMgr();
    virtual ~ResourceMgr() = default;

private:
    boost::asio::io_service m_IOService;
    std::size_t m_ThreadPoolSize = std::thread::hardware_concurrency() * 2; // TODO: make configurable
    boost::asio::signal_set m_Signals;
    std::vector<std::function<void()>> m_OnStopHandlers;
};
