/* 
 * File:   RouteTable.h
 * Author: archimed
 *
 * Created on 27 августа 2018 г., 16:03
 */

#pragma once

#include <unordered_map>
#include <boost/thread.hpp>
#include <boost/noncopyable.hpp>
#include "protocol/Definitions.h"
#include "tcp/Client.h"

class RouteTable : private boost::noncopyable
{
public:

    struct route
    {
        const std::string host;
        const std::string port;

        bool operator<(const route& that) const
        {
            return this->host < that.host || this->port < that.port;
        }
    };

    static RouteTable& instance()
    {
        static RouteTable _inst;
        return _inst;
    }

    void update(const std::map<std::uint64_t, struct route>& routes);
    void route(std::uint64_t id, const protocol::t_Buffer& msg);
    bool isValidId(std::uint64_t id) const;
    void stopAll();

private:
    RouteTable();
    virtual ~RouteTable() = default;
private:
    using t_HostPortKey = struct route;
    using t_ClientPtr = std::shared_ptr<tcp::client::Client>;

    mutable boost::shared_mutex m_Lock; // use std::shared_mutex for C++17
    std::map<t_HostPortKey, t_ClientPtr> m_Clients;
    std::unordered_map<std::uint64_t, t_ClientPtr> m_Routes;
};
