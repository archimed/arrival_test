/* 
 * File:   TLVParser.cpp
 * Author: archimed
 * 
 * Created on 25 августа 2018 г., 21:38
 */

#include <memory>
#include <algorithm>
#include <iostream>
#include "protocol/TLV/TLVParser.h"
#include "protocol/TLV/BasicTLVPackage.h"
#include "protocol/TLV/Types/Types.h"
#include "protocol/TLV/Types/Messages.h"


namespace protocol
{

std::vector<std::unique_ptr<BasicTLVPackage>> TLVParser::proceed(t_ReceiveBuffer::const_iterator begin, t_ReceiveBuffer::const_iterator end)
{
    m_Buffer.insert(m_Buffer.end(), begin, end); // TODO: to iterators

    std::vector<std::unique_ptr < BasicTLVPackage>> result;
    BufferHolder offset(m_Buffer);

    while (true)
    {
        std::size_t count = 0;
        if (m_Buffer.begin() + offset + count >= m_Buffer.end())
        {
            break;
        }

        BasicTLVPackage::Header hdr;
        count += helpers::unpack(m_Buffer.begin() + offset + count, m_Buffer.end(), hdr.tag);
        count += helpers::unpack(m_Buffer.begin() + offset + count, m_Buffer.end(), hdr.length);
        if (m_Buffer.begin() + offset + count + hdr.length > m_Buffer.end())
        {
            break;
        }

        switch (hdr.tag)
        {
        case PackageType::e_Msg1:
        {
            auto msg = std::make_unique<Msg1>();
            count += msg->decodeValue(m_Buffer.begin() + offset + count, m_Buffer.begin() + offset + count + hdr.length);
            offset += count;
            result.emplace_back(std::move(msg));
            std::cout << "Received Msg1" << std::endl;
            break;
        }
        case PackageType::e_Msg2:
        {
            auto msg = std::make_unique<Msg2>();
            count += msg->decodeValue(m_Buffer.begin() + offset + count, m_Buffer.begin() + offset + count + hdr.length);
            offset += count;
            result.emplace_back(std::move(msg));
            std::cout << "Received Msg2" << std::endl;
            break;
        }
        default:
            throw std::runtime_error("unknown packge"); // FIXME: own exc
        }
    }

    return result;
}


}