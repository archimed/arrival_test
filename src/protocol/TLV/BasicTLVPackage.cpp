/* 
 * File:   BasicTLVPackage.cppA
 * Author: archimed
 *
 * Created on 26 августа 2018 г., 2:17
 */

#include <boost/endian/conversion.hpp>

#include "BasicTLVPackage.h"

namespace protocol
{

namespace helpers
{

template<> std::uint16_t ntoh<std::uint16_t>(const std::uint16_t& v)
{
    return ntohs(v);
}

template<> std::uint64_t ntoh<std::uint64_t>(const std::uint64_t& v)
{
    return boost::endian::big_to_native(v);
}

template<> std::uint16_t hton<std::uint16_t>(const std::uint16_t& v)
{
    return htons(v);
}

template<> std::uint64_t hton<std::uint64_t>(const std::uint64_t& v)
{
    return boost::endian::native_to_big(v);
}

}

}
