#pragma once

#include <stdexcept>
#include <type_traits>
#include <iterator>
#include <arpa/inet.h>
#include "protocol/Definitions.h"

namespace protocol
{

namespace helpers
{
template<class T> T ntoh(const T& v) = delete; // force template specialization
template<class T> T hton(const T& v) = delete; // force template specialization

template<> std::uint16_t ntoh<std::uint16_t>(const std::uint16_t& v);
template<> std::uint64_t ntoh<std::uint64_t>(const std::uint64_t& v);

template<> std::uint16_t hton<std::uint16_t>(const std::uint16_t& v);
template<> std::uint64_t hton<std::uint64_t>(const std::uint64_t& v);

template<class T, class it>
static std::size_t unpack(const it& begin, const it& end, T& value)
{
    static_assert(std::is_pod<T>::value, "use unpack function only for POD types");

    auto distance = std::distance(begin, end);
    if (distance < 0 || static_cast<std::size_t> (distance) < sizeof (T))
        throw std::runtime_error(std::string("invalid distance \"").
                                 append(std::to_string(distance)).
                                 append("\", required \"").
                                 append(std::to_string(sizeof (T))).
                                 append("\"").
                                 c_str()); // TODO: make own exceptions

    // For C++17 use typesafe:
    // std::variant<std::uint8_t[sizeof(T)], T> unpacker; 

    union
    {
        std::uint8_t a[sizeof (T)];
        T b;
    } unpacker;

    std::copy(begin, begin + sizeof (unpacker.a), unpacker.a);
    value = ntoh(unpacker.b);

    return sizeof (unpacker.b);
}

template<class T, class it>
static std::size_t pack(it pos, const T& value)
{
    static_assert(std::is_pod<T>::value, "use unpack function only for POD types");

    union
    {
        std::uint8_t a[sizeof (T)];
        T b;
    } packer;

    packer.b = hton(value);
    std::copy(packer.a, packer.a + sizeof (T), pos);

    return sizeof (packer.b);
}
}

class BasicTLVPackage
{
public:

    struct Header
    {
        std::uint16_t tag;
        std::uint16_t length;
    };

    virtual std::uint16_t getTag() const = 0;
    virtual std::uint16_t getLength() const = 0;

    constexpr std::uint16_t getHeaderLength() const
    {
        return sizeof (Header::tag) + sizeof (Header::length);
    };

    virtual t_Buffer pack() const = 0;
};


using t_TLVHeader = BasicTLVPackage::Header;

}
