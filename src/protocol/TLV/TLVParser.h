/* 
 * File:   TLVParser.h
 * Author: archimed
 *
 * Created on 25 августа 2018 г., 21:38
 */

#pragma once

#include <array>
#include <memory>
#include <vector>
#include "protocol/Definitions.h"
#include "protocol/TLV/BasicTLVPackage.h"

namespace protocol
{

class TLVParser
{

    class BufferHolder
    {
    public:

        BufferHolder(std::vector<uint8_t>& hold) : m_Hold(hold) { }

        ~BufferHolder()
        {
            auto size = m_Hold.size();
            auto cutTo = size - m_Count;
            std::copy(m_Hold.begin() + m_Count, m_Hold.end(), m_Hold.begin());
            m_Hold.resize(cutTo);
        }

        operator std::size_t() const
        {
            return m_Count;
        }

        BufferHolder& operator+=(std::size_t n)
        {
            m_Count += n;
            return *this;
        }
    private:
        std::vector<uint8_t>& m_Hold;
        std::size_t m_Count = 0;
    };

public:
    TLVParser() = default;
    virtual ~TLVParser() = default;
    std::vector<std::unique_ptr<BasicTLVPackage>> proceed(t_ReceiveBuffer::const_iterator begin, t_ReceiveBuffer::const_iterator end);

    std::vector<uint8_t> m_Buffer;
};

}