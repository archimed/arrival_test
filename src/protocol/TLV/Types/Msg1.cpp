/* 
 * File:   Msg1.cpp
 * Author: archimed
 * 
 * Created on 26 августа 2018 г., 9:18
 */

#include "Msg1.h"

namespace protocol
{

std::size_t Msg1::decodeValue(t_Buffer::const_iterator begin, t_Buffer::const_iterator end)
{
    return helpers::unpack(begin, end, m_ClientID);
}

t_Buffer Msg1::pack() const
{
    t_Buffer res;
    res.resize(getHeaderLength() + getLength());

    std::size_t i = 0;
    i += helpers::pack(res.begin() + i, getTag());
    i += helpers::pack(res.begin() + i, getLength());
    helpers::pack(res.begin() + i, m_ClientID);

    return std::move(res);
}

std::uint16_t Msg1::getLength() const
{
    return sizeof (m_ClientID);
}

}
