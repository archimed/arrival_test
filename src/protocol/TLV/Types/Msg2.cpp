/* 
 * File:   Msg2.cpp
 * Author: archimed
 * 
 * Created on 27 августа 2018 г., 14:33
 */

#include <iterator>
#include <vector>
#include <algorithm>

#include "Msg2.h"

namespace protocol
{

std::size_t Msg2::decodeValue(t_Buffer::const_iterator begin, t_Buffer::const_iterator end)
{
    auto distance = std::distance(begin, end);
    if (distance < 0)
        throw std::runtime_error("wrong iterators"); // FIXME: own exc

    m_Data.resize(distance);
    std::copy(begin, end, m_Data.begin());
    return distance;
}

t_Buffer Msg2::pack() const
{
    t_Buffer res;
    res.resize(getHeaderLength() + getLength());

    std::size_t i = 0;
    i += helpers::pack(res.begin() + i, getTag());
    i += helpers::pack(res.begin() + i, getLength());
    std::copy(m_Data.begin(), m_Data.end(), res.begin() + i); // FIXME: error if m_Data element is not uint8_t

    return std::move(res);
}

std::uint16_t Msg2::getLength() const
{
    return sizeof (decltype(m_Data)::value_type) * m_Data.size();
}

}

