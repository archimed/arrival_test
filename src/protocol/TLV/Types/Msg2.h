/* 
 * File:   Msg2.h
 * Author: archimed
 *
 * Created on 27 августа 2018 г., 14:33
 */

#pragma once

#include "protocol/TLV/BasicTLVPackage.h"
#include "protocol/TLV/Types/Types.h"

namespace protocol
{

class Msg2 : public BasicTLVPackage
{
public:
    Msg2() = default;
    virtual ~Msg2() = default;

    constexpr std::uint16_t getTag() const
    {
        return PackageType::e_Msg2;
    }

    virtual t_Buffer pack() const;
    virtual std::uint16_t getLength() const;
    virtual const std::vector<std::uint8_t>& getData() const
    {
        return m_Data;
    }

    std::size_t decodeValue(t_Buffer::const_iterator begin, t_Buffer::const_iterator end);
private:
    std::vector<std::uint8_t> m_Data;
};

}

