/* 
 * File:   Msg1.h
 * Author: archimed
 *
 * Created on 26 августа 2018 г., 9:18
 */

#pragma once

#include "protocol/TLV/BasicTLVPackage.h"
#include "Types.h"

namespace protocol
{

class Msg1 : public BasicTLVPackage
{
public:
    Msg1() = default;
    virtual ~Msg1() = default;

    constexpr std::uint16_t getTag() const
    {
        return PackageType::e_Msg1;
    }
    std::size_t decodeValue(t_Buffer::const_iterator begin, t_Buffer::const_iterator end);

    virtual t_Buffer pack() const;
    virtual std::uint16_t getLength() const;
    virtual std::uint64_t getId() const
    {
        return m_ClientID;
    }
private:
    std::uint64_t m_ClientID;
};

}