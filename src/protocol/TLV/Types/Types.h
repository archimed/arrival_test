/* 
 * File:   Types.h
 * Author: archimed
 *
 * Created on 27 августа 2018 г., 10:23
 */

#pragma once

namespace protocol
{

enum PackageType
{
    e_Msg1 = 0,
    e_Msg2
};
}