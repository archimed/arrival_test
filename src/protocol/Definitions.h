/* 
 * File:   Definitions.h
 * Author: archimed
 *
 * Created on 25 августа 2018 г., 22:02
 */

#pragma once

#include <vector>
#include <array>
#include <cstdint>

namespace protocol
{
using t_Buffer = std::vector<std::uint8_t>;
const static auto BUFFER_SIZE = 8192;
typedef std::array<std::uint8_t, BUFFER_SIZE> t_ReceiveBuffer;
}