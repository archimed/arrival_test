/* 
 * File:   ResourceMgr.cpp
 * Author: archimed
 * 
 * Created on 27 августа 2018 г., 17:33
 */

#include "ResourceMgr.h"

//==============================================================================

ResourceMgr::ResourceMgr() :
m_Signals(m_IOService)
{
    m_Signals.add(SIGINT);
    m_Signals.add(SIGTERM);
#if defined(SIGQUIT)
    m_Signals.add(SIGQUIT);
#endif
    m_Signals.async_wait([this](boost::system::error_code, int)
    {
        for (auto& i : m_OnStopHandlers)
            i();
    });
}

//==============================================================================

boost::asio::io_service& ResourceMgr::getExecutor()
{
    return m_IOService;
}

//==============================================================================

void ResourceMgr::run()
{
    std::vector<std::unique_ptr<std::thread> > ThreadPool;
    for (std::size_t i = 0; i < m_ThreadPoolSize; ++i)
    {
        ThreadPool.emplace_back(new std::thread([this]()
        {
            m_IOService.run();
        }));
    }

    for (auto &i : ThreadPool)
        i->join();
}

//==============================================================================

void ResourceMgr::registerOnStopCallback(const std::function<void()>& cb)
{
    m_OnStopHandlers.push_back(cb);
}

//==============================================================================
