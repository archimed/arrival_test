/* 
 * File:   Connection.h
 * Author: archimed
 *
 * Created on 25 августа 2018 г., 19:02
 */

#pragma once

#include <memory>

#include <boost/optional.hpp>   
#include <boost/asio.hpp>
#include <boost/noncopyable.hpp>

#include "protocol/TLV/TLVParser.h"
#include "protocol/Definitions.h"

namespace tcp
{
namespace server
{

class Connection : public std::enable_shared_from_this<Connection>, private boost::noncopyable
{
public:
    using t_ConnectionPtr = std::shared_ptr<Connection>;
    explicit Connection(boost::asio::ip::tcp::socket socket, std::function<void(t_ConnectionPtr) > onStopHandler);
    virtual ~Connection() = default;
    void start();
    void stop();

protected:
    void read();
    void proceedPackages(const std::vector<std::unique_ptr<protocol::BasicTLVPackage>>&pkgs);

private:
    std::function<void(t_ConnectionPtr) > m_OnStopHandler;
    boost::asio::ip::tcp::socket m_Socket;
    protocol::t_ReceiveBuffer m_ReceiveBuffer;
    protocol::TLVParser m_StreamParser;

    boost::optional<std::uint64_t> m_StoredClientId = boost::none; // std::optional for C++17
};

using t_ConnectionPtr = Connection::t_ConnectionPtr;

}
}