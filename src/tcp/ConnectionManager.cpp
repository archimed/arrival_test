/* 
 * File:   ConnectionManager.cpp
 * Author: archimed
 * 
 * Created on 25 августа 2018 г., 18:59
 */

#include <vector>
#include <memory>
#include <set>

#include "tcp/ConnectionManager.h"

namespace tcp
{
namespace server
{
//==============================================================================

ConnectionManager::ConnectionManager() { }
//==============================================================================

Connection& ConnectionManager::alllocateClientConnecion(boost::asio::ip::tcp::socket&& socket)
{
    auto onStop = [this](t_ConnectionPtr connection)
    {
        std::lock_guard<std::mutex> _l(m_Lock);
        m_Connections.erase(connection);
    };

    auto connection = std::make_shared<Connection>(std::move(socket), onStop);
    {
        std::lock_guard<std::mutex> _l(m_Lock);
        m_Connections.emplace(connection);
    }
    return *connection;
}

//==============================================================================

void ConnectionManager::stopAll()
{
    for (auto i = m_Connections.begin(); i != m_Connections.end(); ++i)
    {
        auto connection = *i;
        {
            std::lock_guard<std::mutex> _l(m_Lock);
            m_Connections.erase(i);
        }
        connection->stop();
    }
}

//==============================================================================

}
}