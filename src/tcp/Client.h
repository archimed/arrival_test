/* 
 * File:   Client.h
 * Author: archimed
 *
 * Created on 27 августа 2018 г., 17:31
 */

#pragma once

#include <atomic>
#include <memory>
#include <boost/noncopyable.hpp>
#include <boost/asio.hpp>
#include <boost/bind.hpp>

#include "protocol/Definitions.h"

namespace tcp
{
namespace client
{

class Client : public std::enable_shared_from_this<Client>, private boost::noncopyable
{
public:
    Client(boost::asio::io_service& executor, const std::string& server, const std::string& port);
    virtual ~Client();
    void connect();
    void send(const protocol::t_Buffer& buf);
    void askStop();

protected:
    void onResolve(const boost::system::error_code& err, boost::asio::ip::tcp::resolver::iterator endpointIterator);
    void onConnect(const boost::system::error_code& err, boost::asio::ip::tcp::resolver::iterator endpointIterator);
    void onReceive(const boost::system::error_code& err);
    void stop();
    void shutdown();

private:
    const int reconnectInterval = 1;
    
    bool m_IsConnected;
    bool m_NeedStop = false;
    
    boost::asio::ip::tcp::resolver m_Resolver;
    boost::asio::ip::tcp::socket m_Socket;
    const std::string m_Server;
    const std::string m_Port;
    boost::asio::deadline_timer m_ReconnectTimer;
    std::array<char, 4096> m_ReceiveBuffer;
    std::mutex m_Mutex;
};

}
}