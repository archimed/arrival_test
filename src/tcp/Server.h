/* 
 * File:   Server.h
 * Author: archimed
 *
 * Created on 20 августа 2018 г., 21:42
 */

#pragma once

#include <string>
#include <memory>
#include <thread>

#include <boost/asio.hpp>

#include "tcp/Connection.h"
#include "tcp/ConnectionManager.h"

namespace tcp
{
namespace server
{

class Server : public std::enable_shared_from_this<Server>, private boost::noncopyable
{
public:
    explicit Server(boost::asio::io_service& executor,
                    const std::string &address,
                    const std::string &port);
    virtual ~Server() = default;
    void start();

protected:
    void accept();
    void askStop();

private:
    std::string m_Address, m_Port;
    boost::asio::ip::tcp::acceptor m_Acceptor;
    ConnectionManager m_ConnectionManager;
    boost::asio::ip::tcp::socket m_Socket;
};

}
} 