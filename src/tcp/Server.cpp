/* 
 * File:   Server.cpp
 * Author: archimed
 * 
 * Created on 20 августа 2018 г., 21:42
 */

#include <thread>
#include <utility>
#include <signal.h>

#include "tcp/Server.h"
#include "ResourceMgr.h"

namespace tcp
{
namespace server
{

//==============================================================================

Server::Server(boost::asio::io_service& executor,
               const std::string &address,
               const std::string &port) :
m_Address(address), m_Port(port),
m_Acceptor(executor),
m_Socket(executor)
{
    ResourceMgr::instance().registerOnStopCallback([this]()
    {
        askStop();
    });

}

//==============================================================================

void Server::start()
{
    boost::asio::ip::tcp::resolver resolver(ResourceMgr::instance().getExecutor());
    boost::asio::ip::tcp::endpoint endPoint = *resolver.resolve({m_Address, m_Port});
    m_Acceptor.open(endPoint.protocol());
    m_Acceptor.set_option(boost::asio::ip::tcp::acceptor::reuse_address(true));
    m_Acceptor.bind(endPoint);
    m_Acceptor.listen();
    accept();
}

//==============================================================================

void Server::accept()
{
    auto _h(shared_from_this());
    m_Acceptor.async_accept(m_Socket,
                            [this, _h](boost::system::error_code ErrorCode)
                            {
                                if (!m_Acceptor.is_open())
                                    return;
                                if (!ErrorCode)
                                    m_ConnectionManager.alllocateClientConnecion(std::move(m_Socket)).start();
                                accept();
                            });
}

//==============================================================================

void Server::askStop()
{
    m_Acceptor.close();
    m_ConnectionManager.stopAll();
}

//==============================================================================

}
}