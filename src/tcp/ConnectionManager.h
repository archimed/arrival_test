/* 
 * File:   ConnectionManager.h
 * Author: archimed
 *
 * Created on 25 августа 2018 г., 18:59
 */

#pragma once

#include <mutex>
#include <set>
#include <boost/noncopyable.hpp>
#include <boost/asio.hpp>

#include "tcp/Connection.h"

namespace tcp
{
namespace server
{

class ConnectionManager : private boost::noncopyable
{
public:
    ConnectionManager();
    virtual ~ConnectionManager() = default;

    Connection& alllocateClientConnecion(boost::asio::ip::tcp::socket&& socket);
    void stopAll();
private:
    std::set<t_ConnectionPtr> m_Connections;
    std::mutex m_Lock;
};

}
}