/* 
 * File:   Client.cpp
 * Author: archimed
 * 
 * Created on 27 августа 2018 г., 17:31
 */

#include <iostream>
#include <boost/asio.hpp>
#include "Client.h"

namespace tcp
{
namespace client
{

using boost::asio::ip::tcp;

//==============================================================================

Client::Client(boost::asio::io_service& executor, const std::string& server, const std::string& port) :
m_IsConnected(false),
m_Resolver(executor),
m_Socket(executor),
m_Server(server),
m_Port(port),
m_ReconnectTimer(executor) { }

//==============================================================================

Client::~Client()
{
    stop();
}

//==============================================================================

void Client::connect()
{
    // TODO: implement max_tries strategy
    shutdown();

    tcp::resolver::query query(m_Server, m_Port);

    std::unique_lock<std::mutex> lock(m_Mutex);
    auto _h = shared_from_this();
    m_Resolver.async_resolve(query,
                             [this, _h](const boost::system::error_code& err, const tcp::resolver::iterator & it)
                             {
                                 onResolve(err, it);
                             });
}

//==============================================================================

void Client::onResolve(const boost::system::error_code& err, tcp::resolver::iterator endpointIterator)
{
    if (!err)
    {
        std::unique_lock<std::mutex> lock(m_Mutex);
        auto _h = shared_from_this();
        tcp::endpoint endpoint = *endpointIterator;
        m_Socket.async_connect(endpoint,
                               [this, endpointIterator, _h](const boost::system::error_code & err)
                               {
                                   auto i = endpointIterator;
                                   onConnect(err, ++i);
                               });
    }
    else
    {
        std::cout << "Error: " << err.message() << "\n";
    }
}

//==============================================================================

void Client::onConnect(const boost::system::error_code& err, tcp::resolver::iterator endpointIterator)
{
    if (!err)
    {
        std::unique_lock<std::mutex> lock(m_Mutex);
        std::cout << "Connected to " << m_Server << ":" << m_Port << std::endl;
        m_IsConnected = true;
        auto _h = shared_from_this();
        m_Socket.async_receive(boost::asio::buffer(m_ReceiveBuffer),
                               [this, _h](const boost::system::error_code & err, std::size_t)
                               {
                                   onReceive(err);
                               });
    }
    else if (endpointIterator != tcp::resolver::iterator())
    {
        std::unique_lock<std::mutex> lock(m_Mutex);
        m_Socket.close();
        tcp::endpoint endpoint = *endpointIterator;
        auto _h = shared_from_this();
        m_Socket.async_connect(endpoint,
                               [this, endpointIterator, _h](const boost::system::error_code & err)
                               {
                                   auto i = endpointIterator;
                                   onConnect(err, ++i);
                               });
    }
    else if (err != boost::asio::error::operation_aborted)
    {
        std::unique_lock<std::mutex> lock(m_Mutex);
        std::cout << "Error: " << err.message() << ", reconnect after " << reconnectInterval << " seconds" << std::endl;
        auto _h = shared_from_this();
        m_ReconnectTimer.expires_from_now(boost::posix_time::seconds(reconnectInterval));
        m_ReconnectTimer.async_wait([this, _h](const boost::system::error_code & err)
        {
            if (!err && !m_NeedStop)
                connect();
        });
    }

}

//==============================================================================

void Client::onReceive(const boost::system::error_code& err)
{
    if ((err == boost::asio::error::eof) || (err == boost::asio::error::connection_reset))
    {
        std::cout << "Client " << m_Server << ":" << m_Port << " disconnected, reconnect after " << reconnectInterval << " seconds" << std::endl;

        std::unique_lock<std::mutex> lock(m_Mutex);
        auto _h = shared_from_this();
        m_ReconnectTimer.expires_from_now(boost::posix_time::seconds(reconnectInterval));
        m_ReconnectTimer.async_wait([this, _h](const boost::system::error_code & err)
        {
            if (!err && !m_NeedStop)
                connect();
        });
    }
    else if (err != boost::asio::error::operation_aborted)
    {
        // Just skip data, we wait nothing from client
        std::unique_lock<std::mutex> lock(m_Mutex);
        auto _h = shared_from_this();
        m_Socket.async_receive(boost::asio::buffer(m_ReceiveBuffer),
                               [this, _h](const boost::system::error_code & err, std::size_t)
                               {
                                   onReceive(err);
                               });
    }
    else
    {
        std::cout << "Error: " << err.message() << "\n";
    }
}

//==============================================================================

void Client::send(const protocol::t_Buffer& buf)
{
    std::unique_lock<std::mutex> lock(m_Mutex);

    if (!m_IsConnected)
        throw std::runtime_error("Not connected");

    auto buffer = boost::asio::buffer(buf.data(), buf.size());

    auto _h = shared_from_this();
    boost::asio::async_write(m_Socket, buffer,
                             [this, _h](const boost::system::error_code & err, const long unsigned int)
                             {
                                 if (err)
                                 {
                                     std::cerr << "Error while sending message: " << err << std::endl;
                                     connect();
                                 }
                             });
}

//==============================================================================

void Client::askStop()
{
    std::cout << "Shutting down " << m_Server << ":" << m_Port << "..." << std::endl;

    {
        std::unique_lock<std::mutex> lock(m_Mutex);
        m_IsConnected = false;
        m_NeedStop = true;
        boost::system::error_code err(boost::asio::error::operation_aborted);
        m_ReconnectTimer.cancel(err);
        m_Socket.shutdown(tcp::socket::shutdown_both, err);
    }
}

//==============================================================================

void Client::stop()
{
    shutdown();
    std::cout << "Stopped: " << m_Server << ":" << m_Port << "..." << std::endl;
}

//==============================================================================

void Client::shutdown()
{
    std::unique_lock<std::mutex> lock(m_Mutex);

    m_Socket.close();
    m_Resolver.cancel();
}

//==============================================================================


}
}