/* 
 * File:   Connection.cpp
 * Author: archimed
 * 
 * Created on 25 августа 2018 г., 19:02
 */

#include <iostream>

#include "tcp/Connection.h"
#include "RouteTable.h"

#include "protocol/TLV/Types/Types.h"
#include "protocol/TLV/Types/Messages.h"

namespace tcp
{
namespace server
{

//==============================================================================

Connection::Connection(boost::asio::ip::tcp::socket socket, std::function<void(t_ConnectionPtr) > onStopHandler) :
m_OnStopHandler(onStopHandler),
m_Socket(std::move(socket)) { }

//==============================================================================

void Connection::start()
{
    read();
}

//==============================================================================

void Connection::stop()
{
    boost::system::error_code IgnoredErrorCode;
    m_Socket.shutdown(boost::asio::ip::tcp::socket::shutdown_both, IgnoredErrorCode);
    m_Socket.close();
    m_OnStopHandler(shared_from_this());
}

//==============================================================================

void Connection::read()
{
    auto _h = shared_from_this();
    m_Socket.async_read_some(boost::asio::buffer(m_ReceiveBuffer),
                             [this, _h](boost::system::error_code ErrorCode, std::size_t nByteCount)
                             {
                                 if (!ErrorCode)
                                 {
                                     try
                                     {
                                         auto parsedPackages = m_StreamParser.proceed(m_ReceiveBuffer.begin(), m_ReceiveBuffer.begin() + nByteCount);
                                         proceedPackages(parsedPackages);
                                         read();
                                     }
                                     catch (const std::exception& exc)
                                     {
                                         std::cerr << "handeled an exception: " << exc.what() << std::endl;
                                         stop();
                                     }
                                 }
                                 else if (ErrorCode != boost::asio::error::operation_aborted)
                                 {
                                     stop();
                                 }
                             });
}

//==============================================================================

void Connection::proceedPackages(const std::vector<std::unique_ptr<protocol::BasicTLVPackage>>&pkgs)
{
    for (const auto& i : pkgs)
    {
        switch (i->getTag())
        {
        case protocol::PackageType::e_Msg1:
        {
            auto& msg1 = static_cast<protocol::Msg1&> (*i);
            m_StoredClientId = msg1.getId();
            if (!RouteTable::instance().isValidId(m_StoredClientId.get()))
            {
                std::cerr << "Wrong client id" << m_StoredClientId .get() << std::endl;
                stop();
            }
            else
            {
                std::cout << "Switch route for client " << m_StoredClientId .get() << std::endl;
            }
            break;
        }
        case protocol::PackageType::e_Msg2:
        {
            if (!m_StoredClientId)
                throw std::runtime_error("ClientId was not set");
            RouteTable::instance().route(m_StoredClientId.get(), std::move(i->pack()));
            break;
        }
        default:
        {
            std::cout << "Unknown message from client" << std::endl;
            stop();
        }
        }
    }
}

//==============================================================================

}
}